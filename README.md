# GitLab's Integrate Kubernetes cluster automation


This GitLab project template uses GitLab's Kubernetes integration feature, accessed
from *Your-Project >> Operations >> Kubernetes*.

This template used the GitLab Kubernetes integration feature by integrating a 
new Kubernetes cluster under an existing GKE project. 

After the Kubernetes cluster is integrated, GitLab provides tooling that can be 
insalled on the newly integrated cluster using a Helm client provided as part of
the integration feature.

The integration also provides a GitLab Runner with a Kubernetes Executor. This 
runner can be accessed in the usual Gitlab runner UI, located at 
*Your-Project >> Settings >> CI / CD >> Runners*.  To ensure the Kubernetes Runner
is used, be sure to disable the GitLab shared Runners.

A Sonarqube server's URL is required for GitLab configuration as well a user's 
credentials to access the instance. A Sonarqube project configuration file 
`sonar-project.properties`, placed in the GitLab project root directory for the 
Sonar scanner client to access and provide reports to the Sonarqube project.